import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class MarsExploration {

    // Complete the marsExploration function below.
    static int marsExploration(String s) {

        

		int result = 0;
		
		char[] cArr = s.toCharArray();
		for(int i=0; i<s.length(); i=i+3) {
			if(!String.valueOf(cArr[i]).equals("S")) result++;
			if(!String.valueOf(cArr[i+1]).equals("O")) result++;
			if(!String.valueOf(cArr[i+2]).equals("S")) result++;
		}
		
		return result;

    

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String s = scanner.nextLine();

        int result = marsExploration(s);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}

