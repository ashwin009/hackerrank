import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
    
    private static String numbers = "0123456789";
	private static String lower_case = "abcdefghijklmnopqrstuvwxyz";
	private static String upper_case = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static String special_characters = "!@#$%^&*()-+";
	private static int MIN_LENGTH = 6;

    static int minimumNumber(int n, String password) {
        // Return the minimum number of characters to make the password strong
      
		int result = 0;
		
			if(!checkContains(lower_case, password.toCharArray())) {
				result++;
			}
			
			if(!checkContains(upper_case, password.toCharArray())) {
				result++;
			}
			
			if(!checkContains(numbers, password.toCharArray())) {
				result++;
			}
			
			if(!checkContains(special_characters, password.toCharArray())) {
				result++;
			}
			
        int len = (password.length() + result);
		if(len < 6) {
			result = result + (6-len);
		}
		
		return result;
    
    }
    
    private static boolean checkContains(String types, char[] password) {
		// TODO Auto-generated method stub
		
		boolean result = false;
		for(char c: password) {
			if(types.contains(String.valueOf(c))) {
				result = true;
			}
		}
		return result;
	}

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        String password = in.next();
        int answer = minimumNumber(n, password);
        System.out.println(answer);
        in.close();
    }
}