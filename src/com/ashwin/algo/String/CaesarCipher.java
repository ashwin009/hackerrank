import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;

public class CaesarCipher {

    // Complete the caesarCipher function below.
    
    static String caesarCipher(String s, int k) {

		// lowercase a-z -> 97-122
		// uppercase A-Z -> 65-90
		
		StringBuilder sb = new StringBuilder();
		
		char[] cArr = s.toCharArray();
		
		for(int i=0; i<cArr.length; i++) {
			
			char curr = cArr[i];
			int ascii = (int)curr;
			
				// uppercase
			if(ascii > 64 && ascii < 91){
			
				ascii = ascii + k;
				while(ascii > 90) {
					ascii = ascii - 90 + 64;
				}
				sb.append(String.valueOf((char)ascii));
				
				
				// lowercase
			}else if(ascii > 96 && ascii < 123) {
				
				ascii = ascii + k;
				while(ascii > 122) {
					ascii = ascii - 122 + 96;
				}
				sb.append(String.valueOf((char)ascii));
				
			}else {
				
				sb.append(String.valueOf(curr));
			}
		}
		
		return sb.toString();

    

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        String s = scanner.nextLine();

        int k = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        String result = caesarCipher(s, k);

        bufferedWriter.write(result);
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }
}

